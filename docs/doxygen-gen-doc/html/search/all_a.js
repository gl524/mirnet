var searchData=
[
  ['releasedfromribosome',['releasedFromRibosome',['../classmi_r_net_1_1m_r_n_a.html#a3f6f99ef677e70373d9f764a433bc157',1,'miRNet::mRNA']]],
  ['ribosome',['Ribosome',['../classmi_r_net_1_1agents_1_1_ribosome.html#aa6cb09032d86b050bdadfdbbf9dc34fe',1,'miRNet::agents::Ribosome']]],
  ['ribosome',['Ribosome',['../classmi_r_net_1_1agents_1_1_ribosome.html',1,'miRNet::agents']]],
  ['riscbinding',['RISCBinding',['../classmi_r_net_1_1m_r_n_a.html#a8fc5c4b6750a6fa6dd025d18fc070aa4',1,'miRNet::mRNA']]],
  ['riscdissociation',['RISCDissociation',['../classmi_r_net_1_1m_r_n_a.html#ad748536aca3ba13b9a2453fabc78deaf',1,'miRNet::mRNA']]],
  ['rna',['RNA',['../classmi_r_net_1_1_r_n_a.html',1,'miRNet']]]
];
