/**
 * 
 */
package miRNet.agents;

import miRNet.mRNA;
import repast.simphony.engine.schedule.ScheduledMethod;
import repast.simphony.space.continuous.ContinuousSpace;

/**
 * @author German Leonov
 *
 * Describes AGO2 mRNA agent-specific methods and functionality.
 */
public class mRNA_AGO2 extends mRNA {
	
	// AGO2 mRNA specific parameters
	protected final int AGO2mRNAMeanLifeSpan = (Integer)parameters.getValue("AGO2mRNAMeanLifeSpan");
	protected final int AGO2ProbabilityToBeSilenced = (Integer)parameters.getValue("AGO2ProbabilityToBeSilenced"); // x in 100 chance for the mRNA to be silenced by RISC
	
	/**
	 * Constructor of the AGO2 mRNA agent.
	 * 
	 * @param space continuous space object where the molecule 
	 * needs to be stored
	 */
	public mRNA_AGO2(ContinuousSpace<Object> space) {
		this.space = space; 
		this.meanLifeSpan = AGO2mRNAMeanLifeSpan;
		this.inCytoplasm = false;
		this.enteredTranslation = false;
		this.targetFoci = null;
		this.atFociLocation = false;
		this.currentTimeAtFoci = 0;
		this.boundByRISC = false;
		this.boundByRibosome = false;
		this.targetRibosome = null;
		this.previousRibosome = null;
	}
	
	/**
	 * Perform an activity every step of the simulation.
	 */
	@ScheduledMethod(start = 1, interval = 1)
	public void activity() {
		mRNAActivity();
	}
	
	/**
	 * Getter method for probabilityToBeSilenced.
	 * 
	 * @return probability of the mRNA being silenced
	 */
	@Override
	public int getProbabilityToBeSilenced() {
		return AGO2ProbabilityToBeSilenced;
	}

}
