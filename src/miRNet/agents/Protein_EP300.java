/**
 * 
 */
package miRNet.agents;

import miRNet.Protein;
import repast.simphony.context.Context;
import repast.simphony.engine.schedule.ScheduledMethod;
import repast.simphony.engine.watcher.Watch;
import repast.simphony.engine.watcher.WatcherTriggerSchedule;
import repast.simphony.random.RandomHelper;
import repast.simphony.space.continuous.ContinuousSpace;
import repast.simphony.space.continuous.NdPoint;
import repast.simphony.util.ContextUtils;

/**
 * @author German Leonov
 *
 * Describes EP300 protein agent-specific methods and functionality.
 * 
 * EP300 protein is produced as a result of EP300 mRNA translation.
 * EP300 protein is then targeted for the nucleus, where it will move
 * randomly until encountering pCREB. Not all EP300 protein is
 * available for pCREB association. pCREB-EP300 will be directed by
 * EP300 (in reality, CREB is responsible for DNA site recognition)
 * to the promoter, where some of the complexes will activate miR-132
 * transcription, producing 1 miR-132 molecule. The EP300 protein is
 * used up and removed from the simulation.
 */
public class Protein_EP300 extends Protein {
	
	// EP300 protein specific parameters accessible to watchers
	public boolean TFComplexBoundPromoter;
	
	// EP300 protein specific parameters, exclusive access to the agent
	private Protein_CREB targetCREB;
	private boolean boundCREB;
	private boolean availableToCREB;
	private int lifeSpan;
	private final int meanLifeSpan = (Integer)parameters.getValue("EP300ProteinMeanLifeSpan");
	private final double deviationOfLifeSpan = 0.1 * meanLifeSpan;
	
	/**
	 * Constructor of the EP300 protein agent.
	 * 
	 * @param space continuous space object where the molecule 
	 * needs to be stored 
	 */
	public Protein_EP300(ContinuousSpace<Object> space) {
		this.space = space;
		this.lifeSpan = 0;
		this.inCytoplasm = true;
		this.boundCREB = false;
		this.targetCREB = null;
		this.targetPromoter = null;
		this.TFComplexBoundPromoter = false;
		this.availableToCREB = false;
	}
	
	/**
	 * Perform an activity every step of the simulation.
	 */
	@ScheduledMethod(start = 1, interval = 1)
	public void activity() {
		
		if (!boundCREB) {
			move();
		} else if (!TFComplexBoundPromoter) {
			moveTowardsPromoter();
		}
		
		lifeSpan ++;
		if (!TFComplexBoundPromoter || !boundCREB) {
			checkLifeSpan(lifeSpan, meanLifeSpan, deviationOfLifeSpan);
		}
	}
	
	/**
	 * Perform the movement activity described for this agent.
	 */
	public void move() {
		
		NdPoint currentLocation = space.getLocation(this);
		double[] currentPosition = new double[] {currentLocation.getX(), currentLocation.getY()};
		double[] newPosition = new double[2];
		double movementVector;
		
		if (!inCytoplasm) {
			do {
				// pick a direction to move
				movementVector = Math.toRadians(RandomHelper.nextDoubleFromTo(0.0, 360.0));
				// check that the new location is within allowed movement space
				newPosition[0] = currentPosition[0] + movementRate * Math.cos(movementVector);
				newPosition[1] = currentPosition[1] + movementRate * Math.sin(movementVector);

			} while (!locatedInNucleus(newPosition[0], newPosition[1]));
		} else {
			// move towards the (centre) nucleus
			NdPoint targetLocation = new NdPoint(nucleusCenter, nucleusCenter);
			
			double[] newMovementResult = findNewLocationByDirectedMovement(space, currentLocation, movementRate, targetLocation);
			newPosition[0] = newMovementResult[0];
			newPosition[1] = newMovementResult[1];
			movementVector = newMovementResult[2];
			
			if (locatedInNucleus(newPosition[0], newPosition[1])) {
				inCytoplasm = false;
			}
		}
		space.moveByVector(this, movementRate, movementVector, 0);
	}
	
	/**
	 * Move towards the promoter. This method also moves the bound CREB.
	 */
	public void moveTowardsPromoter() {
		if (targetPromoter == null) {
			targetPromoter = getPromoter();
		}
		NdPoint currentLocation = space.getLocation(this);
		NdPoint promoterLocation = space.getLocation(targetPromoter);
		
		// calculate allowed movement towards the promoter
		double[] movementResult = findNewLocationByDirectedMovement(space, currentLocation, movementRate, promoterLocation);
		
		// bind promoter if close enough, otherwise move towards it
		if (movementRate >= movementResult[3]) {
			space.moveTo(this, promoterLocation.getX(), promoterLocation.getY());
			space.moveTo(targetCREB, promoterLocation.getX(), promoterLocation.getY());
			TFComplexBoundPromoter = true;
			performOverTimeAction("eventsPostTranscriptionalActivation", targetPromoter.getMiRNATranscriptionTime());
		} else {
			while (!locatedInNucleus(movementResult[0], movementResult[1])) {
				double[] newMovementResult = findNewLocationByDirectedMovement(space, currentLocation, movementRate, promoterLocation);
				movementResult = newMovementResult;
			}
		// make the required move towards the promoter
		space.moveByVector(this, movementRate, movementResult[2], 0);
		space.moveByVector(targetCREB, movementRate, movementResult[2], 0);
		}
	}
	
	/**
	 * Watches the binding of pCREB to the EP300 protein.
	 */
	@Watch(watcheeClassName = "miRNet.agents.Protein_CREB", 
			watcheeFieldNames = "boundEP300",
			triggerCondition = "$watchee.boundEP300 == true",
			query = "within 0.0001", 
			whenToTrigger = WatcherTriggerSchedule.IMMEDIATE)
	public void CREBbinding(Protein_CREB targetProtein_CREB) {
		targetCREB = targetProtein_CREB;
		boundCREB = true;
	}
	
	/**
	 * Removes used up agents after transcription has been triggered.
	 * 
	 * Places a new CREB protein into the cytoplasm to replace the
	 * removed CREB from the nucleus. The new CREB is made unphosphorylated
	 * and mimics the possibility of CREB dephosphorylation in the nucleus
	 * and its transport to the cytoplasm.
	 */
	public void eventsPostTranscriptionalActivation() {
		Context<Object> context = ContextUtils.getContext(this);
		
		// EP300 and associated CREB are removed from the cell
		context.remove(this);
		context.remove(targetCREB);
		
		// to keep the population of CREB constant and simulate
		// possible CREB dephosphorylation, create a new CREB 
		context.add(new Protein_CREB(space));
	}
	
	/**
	 * Getter to report if the protein is available for CREB binding. 
	 * 
	 * @return availableToCREB
	 */
	public boolean getAvailableToCREB() {
		return availableToCREB;
	}
	
	/**
	 * Getter to report if the protein cellular location. 
	 * 
	 * @return true if in cytoplasm
	 */
	public boolean getInCytoplasm() {
		return inCytoplasm;
	}
	
	/**
	 * Getter to report if the protein is bound to CREB. 
	 * 
	 * @return true if bound to CREB
	 */
	public boolean getBoundCREB() {
		return boundCREB;
	}
	
	/**
	 * Set the protein is available for CREB binding.
	 * 
	 * @param availability set the availability of EP300 to CREB
	 */
	public void setAvailableToCREB(boolean availability) {
		availableToCREB = availability;
	}
}
