/**
 * 
 */
package miRNet.agents;

import miRNet.mRNA;
import repast.simphony.engine.schedule.ScheduledMethod;
import repast.simphony.space.continuous.ContinuousSpace;

/**
 * @author German Leonov
 *
 * Describes EP300 mRNA agent-specific methods and functionality.
 */
public class mRNA_EP300 extends mRNA {
	
	// EP300 mRNA specific parameters
	protected final int EP300mRNAMeanLifeSpan = (Integer)parameters.getValue("EP300mRNAMeanLifeSpan");
	protected final int EP300ProbabilityToBeSilenced = (Integer)parameters.getValue("EP300ProbabilityToBeSilenced"); // x in 100 chance for the mRNA to be silenced by RISC
	
	/**
	 * Constructor of the EP300 mRNA agent.
	 * 
	 * @param space continuous space object where the molecule 
	 * needs to be stored
	 */
	public mRNA_EP300(ContinuousSpace<Object> space) {
		this.space = space;
		this.meanLifeSpan = EP300mRNAMeanLifeSpan;
		this.inCytoplasm = false;
		this.enteredTranslation = false;
		this.targetFoci = null;
		this.atFociLocation = false;
		this.currentTimeAtFoci = 0;
		this.boundByRISC = false;
		this.boundByRibosome = false;
		this.targetRibosome = null;
		this.previousRibosome = null;
	}
	
	/**
	 * Perform an activity every step of the simulation.
	 */
	@ScheduledMethod(start = 1, interval = 1)
	public void activity() {
		mRNAActivity();
	}
	
	/**
	 * Getter method for probabilityToBeSilenced.
	 * 
	 * @return probability of the mRNA being silenced
	 */
	@Override
	public int getProbabilityToBeSilenced() {
		return EP300ProbabilityToBeSilenced;
	}

}
