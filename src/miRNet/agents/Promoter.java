/**
 * 
 */
package miRNet.agents;

import miRNet.Component;
import repast.simphony.context.Context;
import repast.simphony.engine.schedule.ScheduledMethod;
import repast.simphony.engine.watcher.Watch;
import repast.simphony.engine.watcher.WatcherTriggerSchedule;
import repast.simphony.essentials.RepastEssentials;
import repast.simphony.random.RandomHelper;
import repast.simphony.space.continuous.ContinuousSpace;
import repast.simphony.space.continuous.NdPoint;
import repast.simphony.util.ContextUtils;

/**
 * @author German Leonov
 *
 * Describes miR-132 promoter agent-specific methods and functionality.
 * 
 * Agent characteristics:
 * 		(1) immobile
 * 		(2) exists for the full duration of the simulation
 * 		(3) exists as a single instance
 *		(4) located in the nucleus
 *		(5) responds to TF-complex binding to initiate transcription
 *
 * PMA-trigger funcitionality added to the promoter agent as it is the main responder agent.
 */
public class Promoter extends Component {
	
	// promoter specific parameters, exclusive access to the agent
	private int miR221TranscriptionCopy;
	private double miR221TranscriptionSum;
	private int AGO2TranscriptionCopy;
	private double AGO2TranscriptionSum;
	private int EP300TranscriptionCopy;
	private double EP300TranscriptionSum;
	
	private int miRNATranscriptionTime;
	private int mRNATranscriptionTime;
	private double miR221TranscriptionInterval;
	private double EP300TranscriptionInterval;
	private double AGO2TranscriptionInterval;
	private double PMATreatmentStart;
	private double miR221PMAEffectTranscriptionMultiplier;
	private double AGO2PMAEffectTranscriptionMultiplier;
	
	private final int pCREB_EP300AvailableForMiR132Transcription = 
			(Integer)parameters.getValue("pCREB_EP300AvailableForMiR132Transcription"); // x in 1000 of pCREB-EP300 available for miR-132 transcription
	
	/**
	 * @param space continuous space object where the molecule 
	 * needs to be stored
	 */
	public Promoter(ContinuousSpace<Object> space) {
		this.space = space;
		this.miRNATranscriptionTime = (Integer)parameters.getValue("miRNATranscriptionTime");
		this.mRNATranscriptionTime = (Integer)parameters.getValue("mRNATranscriptionTime");
		this.miR221TranscriptionInterval = (Integer)parameters.getValue("miR221TranscriptionInterval");
		this.EP300TranscriptionInterval = (Integer)parameters.getValue("EP300TranscriptionInterval");
		this.AGO2TranscriptionInterval = (Integer)parameters.getValue("AGO2TranscriptionInterval");
		
		this.PMATreatmentStart = (Integer)parameters.getValue("PMATreatmentStart");
		
		this.miR221PMAEffectTranscriptionMultiplier = (Double)parameters.getValue("miR221PMAEffectTranscriptionMultiplier");
		
		this.miR221TranscriptionCopy = 0;
		this.miR221TranscriptionSum = 0;
		this.AGO2TranscriptionCopy = 0;
		this.AGO2TranscriptionSum = 0;
		this.EP300TranscriptionCopy = 0;
		this.EP300TranscriptionSum = 0;
	}
	
	/**
	 * Perform an activity every step of the simulation.
	 * 
	 * Transcription of mRNA and miR221 depend on the interval at which they
	 * should be produced; PMA treatment will affect this interval.
	 */
	@ScheduledMethod(start = 1, interval = 1)
	public void activity() {
		
		double currentTime = RepastEssentials.GetTickCount();
		double PMATreatmentStart = (Integer)parameters.getValue("PMATreatmentStart");
		double offset = -4.5;
		double timeFromPMAStart = offset - ((currentTime - PMATreatmentStart) / 3600.0);
		
		// miR221 PMA treatment effect function (+miR221PMAEffectTranscriptionMultiplier function approaching the relative change value)
		double b_miR221 = 0.32;
		double n_miR221 = 2.1;
		double miR221Modifier = (b_miR221 * n_miR221 * Math.exp(b_miR221 * timeFromPMAStart) * Math.exp(n_miR221) * Math.exp(-n_miR221 * Math.exp(b_miR221 * timeFromPMAStart))) + miR221PMAEffectTranscriptionMultiplier;
		
		// AGO2 mRNA PMA treatment effect function
		double b_AGO2 = 0.13;
		double n_AGO2 = 3.7;
		double AGO2Modifier = (b_AGO2 * n_AGO2 * Math.exp(b_AGO2 * timeFromPMAStart) * Math.exp(n_AGO2) * Math.exp(-n_AGO2 * Math.exp(b_AGO2 * timeFromPMAStart))) + 1;
		
		// calculate whether to make the next miRNA/mRNA depending on rate of transcription
		miR221TranscriptionSum += 1 / (miR221TranscriptionInterval + activatedByPMA * (miR221TranscriptionInterval / miR221Modifier - miR221TranscriptionInterval));
		AGO2TranscriptionSum += 1 / (AGO2TranscriptionInterval + activatedByPMA * (AGO2TranscriptionInterval / AGO2Modifier - AGO2TranscriptionInterval));
		EP300TranscriptionSum += 1 / (EP300TranscriptionInterval + activatedByPMA * 0);
		
		if (miR221TranscriptionSum >= miR221TranscriptionCopy) {
			performOverTimeAction("miR221Transcription", miRNATranscriptionTime);
			miR221TranscriptionCopy += 1;
		}
		if (AGO2TranscriptionSum >= AGO2TranscriptionCopy) {
			performOverTimeAction("AGO2Transcription", mRNATranscriptionTime);
			AGO2TranscriptionCopy += 1;
		}
		if (currentTime % EP300TranscriptionInterval == 0) {
			performOverTimeAction("EP300Transcription", mRNATranscriptionTime);
		}
		
		// activate PMA state in the cell
		if (currentTime == PMATreatmentStart && PMATreatmentStart != 0) {
			performOverTimeAction("StartPMATreatment", 1);
		}
	}
	
	/**
	 * Watches the TF complex binding to the promoter and in
	 * response initiates miR132 transcription.
	 */
	@Watch(watcheeClassName = "miRNet.agents.Protein_EP300",
			watcheeFieldNames = "TFComplexBoundPromoter",
			triggerCondition = "$watchee.TFComplexBoundPromoter == true",
			query = "within 0.0001",
			whenToTrigger = WatcherTriggerSchedule.IMMEDIATE)
	public void TFComplexBinding() {
		if (pCREB_EP300AvailableForMiR132Transcription >= RandomHelper.nextDoubleFromTo(0, 1000)) {
			performOverTimeAction("miR132Transcription", miRNATranscriptionTime);
		}
		
	}
	
	/**
	 * Method for performing miR132 transcription.
	 */
	public void miR132Transcription() {
		Context<Object> context = ContextUtils.getContext(this);
		NdPoint position = space.getLocation(this);
		miR132 molecule = new miR132(space);
		context.add(molecule);
		space.moveTo(molecule, position.getX(), position.getY());
	}
	
	/**
	 * Method for performing miR221 transcription.
	 */
	public void miR221Transcription() {
		Context<Object> context = ContextUtils.getContext(this);
		NdPoint position = space.getLocation(this);
		miR221 molecule = new miR221(space);
		context.add(molecule);
		space.moveTo(molecule, position.getX(), position.getY());
	}
	
	/**
	 * Method for performing mRNA_EP300 transcription.
	 */
	public void EP300Transcription() {
		Context<Object> context = ContextUtils.getContext(this);
		NdPoint position = space.getLocation(this);
		mRNA_EP300 molecule = new mRNA_EP300(space);
		context.add(molecule);
		space.moveTo(molecule, position.getX(), position.getY());
	}
	
	/**
	 * Method for performing mRNA_AGO2 transcription.
	 */
	public void AGO2Transcription() {
		Context<Object> context = ContextUtils.getContext(this);
		NdPoint position = space.getLocation(this);
		mRNA_AGO2 molecule = new mRNA_AGO2(space);
		context.add(molecule);
		space.moveTo(molecule, position.getX(), position.getY());
	}
	
	/**
	 * Method for triggering PMA activity in the cell.
	 */
	public void StartPMATreatment() {
		activatedByPMA = 1;
	}
	
	/**
	 * Getter for finding out how long transcription takes.
	 * 
	 * @return miRNATranscriptionTime
	 */
	public double getMiRNATranscriptionTime() {
		return miRNATranscriptionTime;
	}
	
	/**
	 * Getter to report on pCREB_EP300AvailableForMiR132Transcription.
	 * 
	 * @return amount out of 1000 of transcription triggering events
	 */
	public int getpCREB_EP300AvailableForMiR132Transcription() {
		return pCREB_EP300AvailableForMiR132Transcription;
	}
	
	
}
