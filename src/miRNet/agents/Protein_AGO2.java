/**
 * 
 */
package miRNet.agents;

import static java.awt.geom.Point2D.distance;
import miRNet.Protein;
import miRNet.mRNA;
import miRNet.miRNA;
import repast.simphony.context.Context;
import repast.simphony.engine.schedule.ScheduledMethod;
import repast.simphony.engine.watcher.Watch;
import repast.simphony.engine.watcher.WatcherTriggerSchedule;
import repast.simphony.query.InstanceOfQuery;
import repast.simphony.random.RandomHelper;
import repast.simphony.space.continuous.ContinuousSpace;
import repast.simphony.space.continuous.NdPoint;
import repast.simphony.util.ContextUtils;

/**
 * @author German Leonov
 *
 * Describes AGO2 protein agent-specific methods and functionality.
 * 
 * AGO2 protein is free to move randomly until bound by a miRNA. Not
 * all of AGO2 protein made is available to miR-132/221. AGO2-miRNA
 * complex, RISC, matures and is able to locate and bind target mRNAs.
 * Not all RISC-miR-132 is used up for suppressing AGO2/EP300 mRNA.
 * Suppression chance is set according to experimental data of the
 * miR:target efficiency of down-regulation. RISC complexes may be
 * re-used for suppressing targets using the same miR-132/AGO2
 * complex. AGO2 governs the movement of the miRNA, although the
 * miRNA provides specificity to target the mRNAs. 
 */
public class Protein_AGO2 extends Protein {
	
	// AGO2 protein specific parameters accessible to watchers
	public boolean boundMRNA;
	
	// AGO2 protein specific parameters, exclusive access to the agent
	private boolean boundCoFactors;
	private boolean boundMiRNA;
	private Object targetMiRNA;
	private Object targetMRNA;
	private Object previousMRNA;
	private int lifeSpan;
	
	private boolean availableToMRNAs;
	private boolean availableToMiRNAs;
	private int silencingCount;
	private final int meanLifeSpan = (Integer)parameters.getValue("AGO2ProteinMeanLifeSpan");
	private final double deviationOfLifeSpan = 0.1 * meanLifeSpan;
	
	private final int miR132RISCAvailableToMRNAs = (Integer)parameters.getValue("miR132RISCAvailableToMRNAs"); // x in 1000 of miR-132-RISC available for EP300/AGO2 mRNA silencing
	private final int coFactorBindingTime = (Integer)parameters.getValue("coFactorBindingTime");
	private final int mRNASilencingTime = (Integer)parameters.getValue("mRNASilencingTime");
	private final int silencingCountLimit = (Integer)parameters.getValue("silencingCountLimit");
	
	
	/**
	 * Constructor of the AGO2 protein agent.
	 * 
	 * @param space continuous space object where the molecule 
	 * needs to be stored
	 */
	public Protein_AGO2(ContinuousSpace<Object> space) {
		this.space = space;
		this.boundCoFactors = false;
		this.boundMiRNA = false;
		this.boundMRNA = false;
		this.targetMiRNA = null;
		this.targetMRNA = null;
		this.previousMRNA = null;
		this.silencingCount = 0;
		this.lifeSpan = 0;
		this.availableToMiRNAs = false;
		this.availableToMRNAs = false;
	}
	
	/**
	 * Perform an activity every step of the simulation.
	 */
	@ScheduledMethod(start = 1, interval = 1)
	public void activity() {
		// check if any miRNA still bound: in the event the miRNA degraded within the complex - reset the bound booleans
		// only check AGO2 proteins that can actually bind any miRNAs in the simulation
		if (availableToMiRNAs && boundMiRNA) {
			if (!findMiRNA()) {
				boundCoFactors = false;
				boundMiRNA = false;
				targetMiRNA = null;
				
				// available AGO2 protein needs to be shortlisted
				AGO2ProteinsAvailableToMiRNAs.add(this);
			}
		}
		
		// perform movement towards the nearest mRNA if this RISC is active and intended for EP300/AGO2 mRNA
		if (boundCoFactors) {
			if (boundMiRNA
					&& targetMiRNA instanceof miR132
					&& availableToMRNAs
					&& !boundMRNA) {
				moveTowardsMRNA();
			}
		}
		
		// if RISC not matured yet, is free, or no target found - move randomly
		if(!boundCoFactors || targetMRNA == null) {
			moveRandomly();
		}
		
		// ensure that AGO2 does not degrade whilst in an active RISC state
		if (!boundMRNA) {
			checkLifeSpan(lifeSpan, meanLifeSpan, deviationOfLifeSpan);
		}
		lifeSpan ++;
	}
	
	/**
	 * Perform the movement activity described for this agent.
	 */
	protected void moveRandomly() {
		NdPoint position = space.getLocation(this);
		double[] currentPosition = new double[] {position.getX(), position.getY()};
		double[] newPosition = new double[2];
		double movementVector;
		
		do {
			// pick a direction to move
			movementVector = Math.toRadians(RandomHelper.nextDoubleFromTo(0.0, 360.0));
			// check that the new location is within allowed movement space
			newPosition[0] = currentPosition[0] + movementRate * Math.cos(movementVector);
			newPosition[1] = currentPosition[1] + movementRate * Math.sin(movementVector);
		} while (locatedInNucleus(newPosition[0], newPosition[1]));
		
		// if miR-bound, make sure to move miR with you
		space.moveByVector(this, movementRate, movementVector, 0);
		if (boundMiRNA) {
			space.moveByVector(targetMiRNA, movementRate, movementVector, 0);
		}
	}
	
	/**
	 * Perform movement towards the nearest mRNA.
	 */
	protected void moveTowardsMRNA() {
		NdPoint currentLocation = space.getLocation(this);
		
		// find nearest mRNA protein
		targetMRNA = findMRNA(space, currentLocation);
		
		if (targetMRNA != null) {
			NdPoint mRNALocation = space.getLocation(targetMRNA);
			
			// calculate allowed movement towards the nearest mRNA
			double[] movementResult = findNewLocationByDirectedMovement(space, currentLocation, movementRate, mRNALocation);
			
			// bind if mRNA is close enough
			if (movementRate >= movementResult[3]) {
				space.moveTo(this, mRNALocation.getX(), mRNALocation.getY());
				space.moveTo(targetMiRNA, mRNALocation.getX(), mRNALocation.getY());
				boundMRNA = true;
				previousMRNA = targetMRNA;
				performOverTimeAction("silencingByRISC", mRNASilencingTime);
			} else {
				while (locatedInNucleus(movementResult[0], movementResult[1])) {
					double[] newMovementResult = findNewLocationByDirectedMovement(space, currentLocation, movementRate, mRNALocation);
					movementResult = newMovementResult;
				}
				space.moveByVector(this, movementRate, movementResult[2], 0);
				space.moveByVector(targetMiRNA, movementRate, movementResult[2], 0);
			}
		}
	}
	
	/**
	 * Triggered by miR-132 binding. Starts RISC maturation.
	 */
	@Watch(watcheeClassName = "miRNet.agents.miR132",
			watcheeFieldNames = "boundAGO2",
			triggerCondition = "$watchee.boundAGO2 == true",
			query = "within 0.0001",
			whenToTrigger = WatcherTriggerSchedule.IMMEDIATE)
	public void miR132Binding(miR132 targetMiR132) {
		boundMiRNA = true;
		targetMiRNA = targetMiR132;
		
		// available AGO2 protein needs to be removed from the shortlist
		AGO2ProteinsAvailableToMiRNAs.remove(this);
		
		if (miR132RISCAvailableToMRNAs >= RandomHelper.nextDoubleFromTo(0, 1000)) {
			availableToMRNAs = true;
		}
		performOverTimeAction("coFactorBinding", coFactorBindingTime);
	}
	
	/**
	 * Triggered by miR-221 binding. Starts RISC maturation.
	 */
	@Watch(watcheeClassName = "miRNet.agents.miR221",
			watcheeFieldNames = "boundAGO2",
			triggerCondition = "$watchee.boundAGO2 == true",
			query = "within 0.0001",
			whenToTrigger = WatcherTriggerSchedule.IMMEDIATE)
	public void miR221Binding(miR221 targetMiR221) {
		boundMiRNA = true;
		targetMiRNA = targetMiR221;
		
		// available AGO2 protein needs to be removed from the shortlist
		AGO2ProteinsAvailableToMiRNAs.remove(this);
		
		performOverTimeAction("coFactorBinding", coFactorBindingTime);
	}
	
	protected Object findMRNA(ContinuousSpace space, NdPoint location) {
		
		double nearestDistance = cytoplasmDiameter;
		Object nearestTarget = null;
		
		// find all the mRNAs
		Context<Object> context = ContextUtils.getContext(this);
		InstanceOfQuery result = new InstanceOfQuery(context, mRNA.class);
		Iterable targets = result.query();
		
		// store the nearest mRNA to the agent
		for (Object target : targets) {
			if (target instanceof mRNA
					&& !((mRNA) target).getEnteredTranslation()
					&& !((mRNA) target).getBoundByRISC()
					&& ((mRNA) target).getInCytoplasm()) {
				NdPoint nearestLocation = space.getLocation(target);
				double newDistance = distance(location.getX(), location.getY(), 
						nearestLocation.getX(), nearestLocation.getY());
				
				if (newDistance < nearestDistance &&
						(mRNA) target != previousMRNA) {
					nearestDistance = newDistance;
					nearestTarget = (mRNA) target;
				}
			}
		}
		return nearestTarget;
	}
	
	/**
	 * Check if miRNA is still present in the simulation (bound to this AGO2).
	 * 
	 * @return miRNA presence in the simulation
	 */
	public boolean findMiRNA() {
		Context<Object> context = ContextUtils.getContext(this);
		if (context.contains(targetMiRNA) && this == ((miRNA) targetMiRNA).getTargetAGO2()) {
			return true;
		}
		return false;
	}
	
	
	/**
	 * Bind co-factors using this method.
	 */
	public void coFactorBinding() {
		boundCoFactors = true;
	}
	
	/**
	 * Silencing by RISC removes the mRNA from the cell.
	 * Silencing is carried out at least once.
	 */
	public void silencingByRISC() {
		Context<Object> context = ContextUtils.getContext(this);
		
		// attempt to silence the mRNA
		if (((mRNA) targetMRNA).getProbabilityToBeSilenced() >= RandomHelper.nextDoubleFromTo(0, 100)) {
			context.remove(targetMRNA);
			silencingCount ++;
		} else {
			targetMRNA = null;
			boundMRNA = false;
		}
		
		// decide what to do with RISC after the attempted silencing
		if (silencingCount >= silencingCountLimit) {
			context.remove(targetMiRNA);
			context.remove(this);
		} else {
			targetMRNA = null;
			boundMRNA = false;
		}
	}
	
	/**
	 * Getter to report if the protein is available for miRNA binding. 
	 * 
	 * @return true if can bind miR-132/221
	 */
	public boolean getAvailableToMiRNAs() {
		return availableToMiRNAs;
	}
	
	/**
	 * Set the protein is available for miRNA binding. 
	 */
	public void setAvailableToMiRNAs(boolean availability) {
		availableToMiRNAs = availability;
	}
	
	/**
	 * Getter to report if the protein is available for mRNA binding.
	 * 
	 * @return true if available for AGO2/EP300 mRNA binding
	 */
	public boolean getAvailableToMRNAs() {
		return availableToMRNAs;
	}
	
	/**
	 * Getter to return the miRNA bound to AGO2.
	 * 
	 * @return miRNA bound to AGO2
	 */
	public Object getTargetMiRNA() {
		return targetMiRNA;
		
	}
	
	/**
	 * Getter to report if the protein has bound miRNA.
	 * 
	 * @return true if bound an miRNA
	 */
	public boolean getBoundMiRNA() {
		return boundMiRNA;
	}
	
	/**
	 * Getter to report if the protein has bound mRNA.
	 * 
	 * @return true if bound an mRNA
	 */
	public boolean getBoundMRNA() {
		return boundMRNA;
	}
	
	/**
	 * Getter to report on AGO2 protein lifespan.
	 * 
	 * @return lifespan of AGO2 protein
	 */
	public double getLifeSpan() {
		return lifeSpan;
	}
	
	/**
	 * Getter to report on silencing count limit.
	 * 
	 * @return limit of silencing attempts
	 */
	public int getsilencingCountLimit() {
		return silencingCountLimit;
	}

}
