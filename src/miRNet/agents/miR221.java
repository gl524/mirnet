/**
 * 
 */
package miRNet.agents;

import miRNet.miRNA;
import repast.simphony.context.Context;
import repast.simphony.engine.schedule.ScheduledMethod;
import repast.simphony.engine.watcher.Watch;
import repast.simphony.engine.watcher.WatcherTriggerSchedule;
import repast.simphony.space.continuous.ContinuousSpace;
import repast.simphony.util.ContextUtils;

/**
 * @author German Leonov
 *
 * Describes miR-221 agent-specific methods and functionality.
 */
public class miR221 extends miRNA {
	
	/**
	 * Constructor of the miR-221 agent.
	 * 
	 * @param space continuous space object where the molecule 
	 * needs to be stored
	 */
	public miR221(ContinuousSpace<Object> space) {
		this.space = space;
		this.lifeSpan = 0;
		this.mature = false;
		this.inCytoplasm = false;
		this.boundAGO2 = false;
		this.targetAGO2 = null;
	}
	
	/**
	 * Perform an activity every step of the simulation.
	 */
	@ScheduledMethod(start = 1, interval = 1)
	public void activity() {
		miRNAActivity();
	}

}