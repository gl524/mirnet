/**
 * 
 */
package miRNet.agents;

import miRNet.Component;
import miRNet.base.DataCrawler;
import repast.simphony.context.Context;
import repast.simphony.engine.watcher.Watch;
import repast.simphony.engine.watcher.WatcherTriggerSchedule;
import repast.simphony.essentials.RepastEssentials;
import repast.simphony.random.RandomHelper;
import repast.simphony.space.continuous.ContinuousSpace;
import repast.simphony.space.continuous.NdPoint;
import repast.simphony.util.ContextUtils;

/**
 * @author German Leonov
 *
 * Describes ribosome agent-specific methods and functionality.
 * 
 * Agent characteristics:
 * 		(1) immobile
 * 		(2) exists for the full duration of the simulation
 * 		(3) many instances present
 *		(4) located in the cytoplasm, randomly dispersed
 *		(5) binds a single mRNA agent at a time
 */
public class Ribosome extends Component {
	
	// ribosome specific parameters accessible to watchers
	public boolean finished;
	
	// ribosome specific parameters, exclusive access to the agent
	private boolean bound;
	private final int AGO2TranslationTime = (Integer)parameters.getValue("AGO2TranslationTime");
	private final int EP300TranslationTime = (Integer)parameters.getValue("EP300TranslationTime");
	private final int EP300AvailableForCREBBinding = (Integer)parameters.getValue("EP300AvailableForCREBBinding"); // x in 1000 of EP300 available for binding 
	private final int AGO2AvailableForMiR132AndMiR221 = (Integer)parameters.getValue("AGO2AvailableForMiR132AndMiR221"); // x in 1000 of AGO2 available for miR-132 and miR-221

	/**
	 * Constructor of the ribosome agent.
	 * 
	 * @param space
	 */
	public Ribosome(ContinuousSpace<Object> space) {
		this.space = space;
		this.bound = false;
		this.finished = false;
	}
	
	/**
	 * Watches the binding of EP300 mRNA to the ribosome and 
	 * in response schedules a translation event.
	 */
	@Watch(watcheeClassName = "miRNet.agents.mRNA_EP300", 
			watcheeFieldNames = "boundByRibosome",
			triggerCondition = "$watchee.boundByRibosome == true",
			query = "within 0.0001", 
			whenToTrigger = WatcherTriggerSchedule.IMMEDIATE)
	public void EP300binding() {
		// set ribosome state and perform action
		bound = true;
		finished = false;
		performOverTimeAction("EP300Translation", EP300TranslationTime);
	}
	
	/**
	 * Watches the binding of EP300 mRNA to the ribosome and 
	 * in activates a translation event.
	 */
	@Watch(watcheeClassName = "miRNet.agents.mRNA_AGO2", 
			watcheeFieldNames = "boundByRibosome",
			triggerCondition = "$watchee.boundByRibosome == true",
			query = "within 0.0001", 
			whenToTrigger = WatcherTriggerSchedule.IMMEDIATE)
	public void AGO2binding() {
		// set ribosome state and perform action
		bound = true;
		finished = false;
		performOverTimeAction("AGO2Translation", AGO2TranslationTime);
	}
	
	/**
	 * Method for performing EP300 translation.
	 */
	public void EP300Translation() {
		// reduce simulation run-time here
		if (EP300AvailableForCREBBinding >= RandomHelper.nextDoubleFromTo(0, 1000)) {
			Context<Object> context = ContextUtils.getContext(this);
			NdPoint position = space.getLocation(this);
			
			// produce a new EP300 protein molecule
			Protein_EP300 molecule = new Protein_EP300(space);
			context.add(molecule);
			space.moveTo(molecule, position.getX(), position.getY());
			
			molecule.setAvailableToCREB(true);
		}
		
		// reset ribosome bound state, triggering mRNA to move
		bound = false;
		finished = true;
	}
	
	/**
	 * Method for performing AGO2 translation.
	 */
	public void AGO2Translation() {
		// reduce simulation run-time here
		if (AGO2AvailableForMiR132AndMiR221 >= RandomHelper.nextDoubleFromTo(0, 1000)) {
			Context<Object> context = ContextUtils.getContext(this);
			NdPoint position = space.getLocation(this);
			
			// produce a new AGO2 protein molecule
			Protein_AGO2 molecule = new Protein_AGO2(space);
			context.add(molecule);
			space.moveTo(molecule, position.getX(), position.getY());
			
			molecule.setAvailableToMiRNAs(true);
			// available AGO2 protein needs to be shortlisted
			AGO2ProteinsAvailableToMiRNAs.add(molecule);
		}
		
		// reset ribosome bound state, triggering mRNA to move
		bound = false;
		finished = true;
	}
	
	/**
	 * Getter to report if ribosome is bound by an mRNA.
	 * 
	 * @return true if it is bound by an mRNA
	 */
	public boolean getIsBound() {
		return bound;
	}
}
