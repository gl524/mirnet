/**
 * 
 */
package miRNet.agents;

import static java.awt.geom.Point2D.distance;

import java.math.BigDecimal;

import miRNet.Protein;
import miRNet.base.DataCrawler;
import repast.simphony.context.Context;
import repast.simphony.engine.schedule.ScheduledMethod;
import repast.simphony.essentials.RepastEssentials;
import repast.simphony.query.InstanceOfQuery;
import repast.simphony.query.space.continuous.ContinuousWithin;
import repast.simphony.random.RandomHelper;
import repast.simphony.space.continuous.ContinuousSpace;
import repast.simphony.space.continuous.NdPoint;
import repast.simphony.util.ContextUtils;

/**
 * @author German Leonov
 *
 * Describes CREB protein agent-specific methods and functionality.
 * 
 * Protein_CREB is instantiated at a free ribosome; corresponding mRNA
 * for production of Protein_CREB has not been implemented for the purpose
 * of this model. CREB has a chance of being phosphorylated and will be
 * transport to the nucleus as pCREB. In the nucleus it will attempt to
 * associate with EP300 protein that is available for CREB binding. If
 * pCREB does not encounter a EP30 protein soon enough, it has a higher
 * chance of being dephosphorylated and exported back into the cytoplasm.
 * pCREB-EP300 complex can activate miR-132 transcription upon promoter
 * binding - not all complexes will be used for that purpose. After the
 * transcriptional activation pCREB is used up (removed) and a substitute
 * CREB molecule reintroduced into the cytoplasm.
 */
public class Protein_CREB extends Protein {
	
	// CREB protein specific parameters accessible to watchers
	public boolean boundEP300;
	
	// CREB protein specific parameters, exclusive access to the agent
	private Protein_EP300 targetEP300;
	private boolean phosphorylated;
	private final double basalPhosphorylationProbability = (Integer)parameters.getValue("basalPhosphorylationProbability"); // x in 10000 chance of being phosphorylated every iteration
	private final double basalDephosphorylationProbability = (Integer)parameters.getValue("basalDephosphorylationProbability"); // x in 10000 chance of being dephosphorylated every iteration

	/**
	 * @param space continuous space object where the molecule 
	 * needs to be stored
	 */
	public Protein_CREB(ContinuousSpace<Object> space) {
		this.space = space;
		this.phosphorylated = false;
		this.inCytoplasm = true;
		this.boundEP300 = false;
		this.targetEP300 = null;
	}
	
	/**
	 * Perform an activity every step of the simulation.
	 */
	@ScheduledMethod(start = 1, interval = 1)
	public void activity() {
		
		if (!boundEP300) {
			if (!phosphorylated && inCytoplasm) {
				phosphorylateByChance(getBasalPhosphorylationProbability());
			} else if (phosphorylated && !inCytoplasm) {
				dephosphorylateByChance(basalDephosphorylationProbability);
			}
			
			move();
		} else {
			// sit back and relax, EP300 will move you
		}
	}
	
	/**
	 * Perform the movement activity described for this agent.
	 */
	public void move() {
	
		NdPoint currentLocation = space.getLocation(this);
		double[] currentPosition = new double[] {currentLocation.getX(), currentLocation.getY()};
		double[] newPosition = new double[2];
		double movementVector;
		
		if (!phosphorylated && inCytoplasm) {
			// move about freely until meeting a kinase
			do {
				// pick a direction to move
				movementVector = Math.toRadians(RandomHelper.nextDoubleFromTo(0.0, 360.0));
				// check that the new location is within allowed movement space
				newPosition[0] = currentPosition[0] + movementRate * Math.cos(movementVector);
				newPosition[1] = currentPosition[1] + movementRate * Math.sin(movementVector);
			} while (locatedInNucleus(newPosition[0], newPosition[1]));
		} else if (phosphorylated && inCytoplasm) {
			// move towards the (centre) nucleus
			NdPoint targetLocation = new NdPoint(nucleusCenter, nucleusCenter);
			
			double[] newMovementResult = findNewLocationByDirectedMovement(space, currentLocation, movementRate, targetLocation);
			newPosition[0] = newMovementResult[0];
			newPosition[1] = newMovementResult[1];
			movementVector = newMovementResult[2];
			
			if (locatedInNucleus(newPosition[0], newPosition[1])) {
				inCytoplasm = false;
			}
		} else if (phosphorylated && !inCytoplasm) {
			// in the nucleus, move towards the nearest EP300 molecule
			targetEP300 = findEP300(space, currentLocation);
			
			if (targetEP300 != null) {
				NdPoint EP300Location = space.getLocation(targetEP300);
				
				do {
					double[] newMovementResult = findNewLocationByDirectedMovement(space, currentLocation, movementRate, EP300Location);
					newPosition[0] = newMovementResult[0];
					newPosition[1] = newMovementResult[1];
					movementVector = newMovementResult[2];
					
					// if EP300 molecule is within the movement distance, move to it and bind
					if (movementRate >= newMovementResult[3]) {
						space.moveTo(this, EP300Location.getX(), EP300Location.getY());
						boundEP300 = true;
						break;
					}
				} while (!locatedInNucleus(newPosition[0], newPosition[1]));
			} else {
				// move randomly if no EP300 molecule in sight
				do {
					// pick a direction to move
					movementVector = Math.toRadians(RandomHelper.nextDoubleFromTo(0.0, 360.0));
					// check that the new location is within allowed movement space
					newPosition[0] = currentPosition[0] + movementRate * Math.cos(movementVector);
					newPosition[1] = currentPosition[1] + movementRate * Math.sin(movementVector);
				} while (!locatedInNucleus(newPosition[0], newPosition[1]));
			}
			
		} else {
			// in the nucleus, not phosphorylated (move away from the (centre) nucleus)
			NdPoint targetLocation = new NdPoint(nucleusCenter, nucleusCenter);
			
			// calculate allowed movement away from nucleus
			double[] movementResult = findNewLocationByDirectedMovement(space, currentLocation, movementRate, targetLocation);
			newPosition[0] = movementResult[0];
			newPosition[1] = movementResult[1];
			movementVector = movementResult[2] + Math.PI; // reverses the movement away from the center (nucleus)
			
			if (!locatedInNucleus(newPosition[0], newPosition[1])) {
				inCytoplasm = true;
			}
		}
		// perform the move unless just bound EP300
		if (!boundEP300) {
			space.moveByVector(this, movementRate, movementVector, 0);
		}
	}
	
	/**
	 * Emulates kinase activity in the cell by marking
	 * CREB as phosphorylated with a certain probability.
	 * 
	 * @param probabilityOfPhosphorylation probability out of 10000 that
	 * CREB gets phosphorylated every iteration
	 */
	public void phosphorylateByChance(double probabilityOfPhosphorylation) {
		int result = RandomHelper.nextIntFromTo(0, 10000);
		if (result < probabilityOfPhosphorylation) {
			phosphorylated = true;
		}
	}
	
	/**
	 * Emulates phosphotase activity in the cell by marking
	 * CREB as dephosphorylated with a certain probability.
	 * 
	 * @param probabilityOfPhosphorylation probability out of 10000 that
	 * CREB gets dephosphorylated every iteration
	 */
	public void dephosphorylateByChance(double probabilityOfDephosphorylation) {
		int result = RandomHelper.nextIntFromTo(0, 10000);
		if (result < probabilityOfDephosphorylation) {
			phosphorylated = false;
		}
	}
	
	/**
	 * Finds the nearest EP300 molecule and begins to follow it.
	 * 
	 * @param space continuous space object where the molecule 
	 * needs to be stored
	 * @param thisAgent the agent from which the surroundings need to be scanned
	 * @param scanDistance the distance to look around from thisAgent
	 * @param location location the current location of thisAgent
	 * @return nearest EP300 molecule
	 */
	public Protein_EP300 findEP300(ContinuousSpace space, NdPoint location) {
		
		double nearestDistance = cytoplasmDiameter;
		Protein_EP300 nearestTarget = null;
		
		// query the space around the agent
		Context<Object> context = ContextUtils.getContext(this);
		InstanceOfQuery result = new InstanceOfQuery(context, Protein_EP300.class);
		Iterable targets = result.query();
		
		// find all the necessary molecules of interest, store the nearest to the agent
		for (Object target : targets) {
			if (target instanceof Protein_EP300 &&
					!((Protein_EP300) target).getInCytoplasm() &&
					!((Protein_EP300) target).getBoundCREB() &&
					((Protein_EP300) target).getAvailableToCREB()) {
				NdPoint nearestLocation = space.getLocation(target);
				double newDistance = distance(location.getX(), location.getY(), 
						nearestLocation.getX(), nearestLocation.getY());
				
				if (newDistance < nearestDistance) {
					nearestDistance = newDistance;
					nearestTarget = (Protein_EP300) target;
				}
			}
		}
		return nearestTarget;
	}
	
	/**
	 * Computes the phosphorylation probability of CREB. Unchanged for the untreated state,
	 * however after the PMA treatment, the probability of CREB phosphorylation is 
	 * dependent on time form the treatment start.
	 * 
	 * @return phosphorylation probability based on the PMA activation state (and time) 
	 */
	public double getBasalPhosphorylationProbability() {
		double currentTime = RepastEssentials.GetTickCount();
		double PMATreatmentStart = (Integer)parameters.getValue("PMATreatmentStart");
		
		double offset = -4.5;
		double b = 0.32;
		double n = 3.75;
		
		double timeFromPMAStart = offset - ((currentTime - PMATreatmentStart) / 3600.0);
		double phosphorylationProbability = basalPhosphorylationProbability * (1 + activatedByPMA * (b * n * Math.exp(b * timeFromPMAStart) * Math.exp(n) * Math.exp(-n * Math.exp(b * timeFromPMAStart))));
		
		return phosphorylationProbability;
	}
	
	/**
	 * Getter to report if the protein is phosphorylated. 
	 * 
	 * @return true if phosphorylated
	 */
	public boolean getPhosphorylated() {
		return phosphorylated;
	}
    
}
