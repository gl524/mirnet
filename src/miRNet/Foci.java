package miRNet;

import repast.simphony.space.continuous.ContinuousSpace;

/**
 * @author German Leonov
 * 
 * Represents a location in space where mRNAs can be stored.
 * Exist as immobile location references, ~10 in a cell. 
 */
public class Foci extends Component {
	
	/**
	 * Constructor of foci where mRNAs are attracted to.
	 * 
	 * @param space continuous space object where the molecule 
	 * needs to be stored
	 */
	public Foci(ContinuousSpace<Object> space) {
		this.space = space;
	}

}
