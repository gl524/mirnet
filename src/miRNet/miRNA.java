/**
 * 
 */
package miRNet;

import static java.awt.geom.Point2D.distance;
import miRNet.agents.Protein_AGO2;
import repast.simphony.context.Context;
import repast.simphony.space.continuous.ContinuousSpace;
import repast.simphony.space.continuous.NdPoint;
import repast.simphony.util.ContextUtils;

/**
 * @author German Leonov
 *
 * Generic functions performed by miRNAs.
 * 
 * miRNAs are processed and picked up by AGO2 upon availability.
 * AGO2 provides a platform for moving the miRNA to the desired
 * location and the miRNA directs the silencing based on target
 * complimentarity. miRNAs bound by AGO2 are much more stable
 * than those that are freely moving in the cell.
 */
public abstract class miRNA extends RNA {
	
	// miRNA specific parameters accessible to watchers
	public boolean boundAGO2;
	
	// miRNA specific parameters accessible to child classes
	protected boolean mature;
	protected Protein_AGO2 targetAGO2;
	
	protected final int movementRate = (Integer)parameters.getValue("miRNAMovementRate");
	protected final int DroshaProcessingTime = (Integer)parameters.getValue("DroshaProcessingTime");
	protected final int DICERProcessingTime = (Integer)parameters.getValue("DICERProcessingTime"); 
	
	protected double lifeSpan;
	protected final int meanLifeSpan = (Integer)parameters.getValue("miRNAMeanLifeSpan");
	protected final double deviationOfLifeSpan = 0.1 * meanLifeSpan;
	protected final double boundAGO2LifeSpanMultiplier = (Double)parameters.getValue("boundAGO2LifeSpanMultiplier");
	
	/**
	 * Describes the generic activity of miRNAs.
	 */
	protected void miRNAActivity() {
		// check AGO2 binding state and ensure the bound state is updated
		if (boundAGO2) {
			if(!findRISC(space)) {
				boundAGO2 = false;
				targetAGO2 = null;
			}
		}
		
		// mature miRNA moves towards AGO2 if possible; once bound to AGO2 it remain within AGO2 movement control
		if (mature) {
			if (!boundAGO2) {
				moveTowardsAGO2();
			}
		} else {
			performOverTimeAction("miRNAMaturation", DroshaProcessingTime + DICERProcessingTime);
		}
		
		// check the lifespan - AGO2 bound miRs are more stable, and cannot degrade when performing silencing
		if (boundAGO2) { 
			if (!targetAGO2.getBoundMRNA()) {
				checkLifeSpan(lifeSpan, boundAGO2LifeSpanMultiplier*meanLifeSpan,
						boundAGO2LifeSpanMultiplier*deviationOfLifeSpan);
			}
		} else {
			checkLifeSpan(lifeSpan, meanLifeSpan, deviationOfLifeSpan);
		}
		lifeSpan ++;
	}
	
	/**
	 * Perform the movement towards AGO2 if possible.
	 */
	protected void moveTowardsAGO2() {
		NdPoint currentLocation = space.getLocation(this);
		
		// find nearest AGO2 protein
		targetAGO2 = findAGO2(space, currentLocation);
		
		if (targetAGO2 != null) {
			NdPoint AGO2Location = space.getLocation(targetAGO2);
			
			// calculate allowed movement towards the nearest AGO2 protein
			double[] movementResult = findNewLocationByDirectedMovement(space, currentLocation, movementRate, AGO2Location);
			
			// bind if AGO2 is close enough
			if (movementRate >= movementResult[3]) {
				space.moveTo(this, AGO2Location.getX(), AGO2Location.getY());
				boundAGO2 = true;
				inCytoplasm = true; // this must be set for coherence - miR can only bind AGO2 in the cytoplasm 
			} else if (inCytoplasm) {
				while (locatedInNucleus(movementResult[0], movementResult[1])) {
					double[] newMovementResult = findNewLocationByDirectedMovement(space, currentLocation, movementRate, AGO2Location);
					movementResult = newMovementResult;
				}
			} else if (!inCytoplasm && !locatedInNucleus(movementResult[0], movementResult[1])) {
				inCytoplasm = true;
			}
			// move if not bound to AGO2
			if (!boundAGO2) {
				space.moveByVector(this, movementRate, movementResult[2], 0);
			}
		} else {
			moveRandomly(movementRate);
		}
	}
	
	/**
	 * Change miRNA maturation state, needed for the molecule scheduling function.
	 */
	public void miRNAMaturation() {
		mature = true;
	}
	
	
	/**
	 * Finds the nearest AGO2 molecule and begins to follow it.
	 * 
	 * @param space continuous space object where the molecule 
	 * needs to be stored
	 * @param location location the current location of thisAgent
	 * @return nearest AGO2 molecule
	 */
	protected Protein_AGO2 findAGO2(ContinuousSpace space, NdPoint location) {
		
		double nearestDistance = cytoplasmDiameter;
		Protein_AGO2 nearestTarget = null;
		
		for (Protein_AGO2 target : AGO2ProteinsAvailableToMiRNAs) {
			NdPoint nearestLocation = space.getLocation(target);
			double newDistance = distance(location.getX(), location.getY(), 
					nearestLocation.getX(), nearestLocation.getY());
			
			if (newDistance < nearestDistance) {
				nearestDistance = newDistance;
				nearestTarget = (Protein_AGO2) target;
			}
		}
		return nearestTarget;
		
		/** OLD Method of looking up !!!
		double nearestDistance = cytoplasmDiameter;
		Protein_AGO2 nearestTarget = null;
		
		// query the space around the agent
		Context<Object> context = ContextUtils.getContext(this);
		InstanceOfQuery result = new InstanceOfQuery(context, Protein_AGO2.class);
		Iterable targets = result.query();
		
		// find all the necessary molecules of interest, store the nearest to the agent
		for (Object target : targets) {
			if (target instanceof Protein_AGO2 &&
					!((Protein_AGO2) target).getBoundMiRNA() &&
					((Protein_AGO2) target).getAvailableToMiRNAs() &&
					((Protein_AGO2) target).getLifeSpan() > 1) { // prevents miRs getting stuck on ribosomes due to AGO2 getting bound upon translation finishing by a miR
				NdPoint nearestLocation = space.getLocation(target);
				double newDistance = distance(location.getX(), location.getY(), 
						nearestLocation.getX(), nearestLocation.getY());
				
				if (newDistance < nearestDistance) {
					nearestDistance = newDistance;
					nearestTarget = (Protein_AGO2) target;
				}
			}
		}
		return nearestTarget;
		*/
	}
	
	/**
	 * Check if AGO2 is still present in the simulation (bound to this miRNA).
	 * 
	 * @return AGO2 protein presence in the simulation
	 */
	protected boolean findRISC(ContinuousSpace space) {
		Context<Object> context = ContextUtils.getContext(this);
		if (context.contains(targetAGO2) && this == targetAGO2.getTargetMiRNA()) {
			return true;
		}
		return false;
	}
	
	/**
	 * Getter to report on the bound AGO2 protein.
	 * 
	 * @return the AGO2 protein this miRNA is bound to
	 */
	public Protein_AGO2 getTargetAGO2() {
		return targetAGO2;
	}
}
