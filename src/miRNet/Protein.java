/**
 * 
 */
package miRNet;

/**
 * @author German Leonov
 *
 * Generic functions performed by proteins.
 */
public abstract class Protein extends Component {
	
	// generic protein parameters
	protected final double movementRate = (Integer)parameters.getValue("proteinMovementRate");

}
