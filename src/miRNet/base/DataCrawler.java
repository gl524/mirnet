
package miRNet.base;

import miRNet.agents.*;
import repast.simphony.context.Context;
import repast.simphony.query.InstanceOfQuery;
import repast.simphony.util.ContextUtils;

/**
 * Gives the dataLoader access to methods for gathering information
 * from the simulation not accessible via count method.
 * 
 * @author German Leonov 
 */
public class DataCrawler {

	// must be added to context to give access of methods to the dataLoader
	public DataCrawler() {
	
	}
	
	/**
	 * Counts phosphorylated CREB molecules.
	 * 
	 * @return phosphorylated CREB count
	 */
	public int countPhosphoCREB() {
		Context<Object> context = ContextUtils.getContext(this);
		InstanceOfQuery result = new InstanceOfQuery(context, Protein_CREB.class);
		int count = 0;
		Iterable targets = result.query();
		for (Object target : targets) {
			if (((Protein_CREB) target).getPhosphorylated()) {
				count++;
				}
		}
		return count;
	}
	
	/**
	 * Counts RISC comeplexes.
	 * 
	 * @return RISC (AGO2 bound miRNA) count
	 */
	public int countRISC() {
		Context<Object> context = ContextUtils.getContext(this);
		InstanceOfQuery result = new InstanceOfQuery(context, Protein_AGO2.class);
		int count = 0;
		Iterable targets = result.query();
		for (Object target : targets) {
			if (((Protein_AGO2) target).getBoundMiRNA()) {
				count++;
				}
		}
		return count;
	}
	
	/**
	 * Counts RISC complexes loaded with miR-132.
	 * 
	 * @return RISC complexes loaded with miR-132
	 */
	public int countRISC132() {
		Context<Object> context = ContextUtils.getContext(this);
		InstanceOfQuery result = new InstanceOfQuery(context, Protein_AGO2.class);
		int count = 0;
		Iterable targets = result.query();
		for (Object target : targets) {
			if (((Protein_AGO2) target).getTargetMiRNA() instanceof miR132) {
				count++;
				}
		}
		return count;
	}
	
	/**
	 * Counts RISC complexes loaded with miR-221.
	 * 
	 * @return RISC complexes loaded with miR-221
	 */
	public int countRISC221() {
		Context<Object> context = ContextUtils.getContext(this);
		InstanceOfQuery result = new InstanceOfQuery(context, Protein_AGO2.class);
		int count = 0;
		Iterable targets = result.query();
		for (Object target : targets) {
			if (((Protein_AGO2) target).getTargetMiRNA() instanceof miR221) {
				count++;
				}
		}
		return count;
	}
	
	/**
	 * Gets EP300NotUsedForCREBBinding for the dataLoader.
	 * 
	 * @return amount of EP300 molecules that were made but not used for CREB binding
	 */
	public int countEP300NotUsedForCREBBinding() {
		Context<Object> context = ContextUtils.getContext(this);
		InstanceOfQuery result = new InstanceOfQuery(context, Protein_EP300.class);
		int count = 0;
		Iterable targets = result.query();
		for (Object target : targets) {
			if (!((Protein_EP300) target).getAvailableToCREB()) {
				count++;
				}
		}
		return count;
	}
	
	/**
	 * Gets EP300UsedForCREBBinding for the dataLoader.
	 * 
	 * @return amount of EP300 molecules that were made for CREB binding
	 */
	public int countEP300UsedForCREBBinding() {
		Context<Object> context = ContextUtils.getContext(this);
		InstanceOfQuery result = new InstanceOfQuery(context, Protein_EP300.class);
		int count = 0;
		Iterable targets = result.query();
		for (Object target : targets) {
			if (((Protein_EP300) target).getAvailableToCREB()) {
				count++;
				}
		}
		return count;
	}
	
	/**
	 * Gets AGO2NotUsedForMiR132AndMiR221 for the dataLoader.
	 * 
	 * @return amount of AGO2 protein not used to bind miR-132 and miR-221
	 */
	public int countAGO2NotUsedForMiR132AndMiR221() {
		Context<Object> context = ContextUtils.getContext(this);
		InstanceOfQuery result = new InstanceOfQuery(context, Protein_AGO2.class);
		int count = 0;
		Iterable targets = result.query();
		for (Object target : targets) {
			if (!((Protein_AGO2) target).getAvailableToMiRNAs()) {
				count++;
				}
		}
		return count;
	}
	
	/**
	 * Gets AGO2UsedForMiR132AndMiR221 for the dataLoader.
	 * 
	 * @return amount of AGO2 protein available to bind miR-132 and miR-221
	 */
	public int countAGO2UsedForMiR132AndMiR221() {
		Context<Object> context = ContextUtils.getContext(this);
		InstanceOfQuery result = new InstanceOfQuery(context, Protein_AGO2.class);
		int count = 0;
		Iterable targets = result.query();
		for (Object target : targets) {
			if (((Protein_AGO2) target).getAvailableToMiRNAs()) {
				count++;
				}
		}
		return count;
	}
	
	/**
	 * Gets RISCUsedForEP300AndAGO2Silencing for the dataLoader.
	 * 
	 * @return amount of miR-132 loaded RISC complexes used for EP300 and AGO2 mRNA silencing
	 */
	public int countRISCUsedForEP300AndAGO2Silencing() {
		Context<Object> context = ContextUtils.getContext(this);
		InstanceOfQuery result = new InstanceOfQuery(context, Protein_AGO2.class);
		int count = 0;
		Iterable targets = result.query();
		for (Object target : targets) {
			if (((Protein_AGO2) target).getTargetMiRNA() instanceof miR132 &&
					((Protein_AGO2) target).getAvailableToMRNAs()) {
				count++;
				}
		}
		return count;
	}
}
