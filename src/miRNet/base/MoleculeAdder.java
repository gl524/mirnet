package miRNet.base;

import miRNet.Foci;
import miRNet.Protein;
import miRNet.RNA;
import miRNet.agents.Promoter;
import miRNet.agents.Ribosome;
import repast.simphony.random.RandomHelper;
import repast.simphony.space.Dimensions;
import repast.simphony.space.continuous.ContinuousAdder;
import repast.simphony.space.continuous.ContinuousSpace;

/**
 * @author gl524
 *
 * Custom adder of agents to the 2D space. Ensures all agents added to
 * their appropriate compartments. Functionality relies on the space 
 * being square shaped.
 * 
 * @param <Object> Object of the simulation that needs to be placed into
 * 2D continuous space.
 */
public class MoleculeAdder<Object> implements ContinuousAdder<Object> {
	
	/**
	 * Adds an object to a specified continuous space. Location is
	 * determined by the instance of the passed object.
	 */
	public void add(ContinuousSpace<Object> space, Object molecule){
		Dimensions dims = space.getDimensions();
		
		// make sure the space is a square
		assert dims.getDimension(0) == dims.getDimension(1);
		
		// derive the location of the nucleus-exlusive area
		double cytoplasmDiameter = dims.getDimension(0);
		double nucleusToCytoplasmRatio = 4;
		double nucleusRadius = (cytoplasmDiameter / nucleusToCytoplasmRatio) / 2;
		double nucleusCenter = cytoplasmDiameter / 2;
		double nucleusEdgeLocationMin = nucleusCenter - nucleusRadius;
		double nucleusEdgeLocationMax = nucleusCenter + nucleusRadius;
		
		// store x, y coordinates in a list
		double[] location = new double[2];
		
		// place agent in correct initial compartment area
		if (molecule instanceof Ribosome ||
			molecule instanceof Protein ||
			molecule instanceof Foci) {
			findLocationInCytoplasm(location, cytoplasmDiameter,
					nucleusEdgeLocationMin, nucleusEdgeLocationMax);
			
		} else if (molecule instanceof RNA ||
				   molecule instanceof Promoter) {
			findLocationInNucleus(location,
					nucleusEdgeLocationMin, nucleusEdgeLocationMax);
		}
		
		space.moveTo(molecule, location);
	}
	
	/**
	 * Finds the location for placing an object in 2D space within the cytoplasm.
	 * 
	 * @param location list that will hold [x, y] coordinates for the new object 
	 * @param cytoplasmDiameter diameter of the cytoplasm
	 * @param nucleusEdgeLocationMin coordinate [x] or [y] that marks the lowest
	 * value of the edge of the nucleus 
	 * @param nucleusEdgeLocationMax coordinate [x] or [y] that marks the highest
	 * value of the edge of the nucleus
	 */
	private void findLocationInCytoplasm(double[] location, double cytoplasmDiameter,
			double nucleusEdgeLocationMin, double nucleusEdgeLocationMax) {
		
		// ensures molecules only added to the cytoplasm
		do {
			location[0] = RandomHelper.nextDoubleFromTo(0, cytoplasmDiameter);
			location[1] = RandomHelper.nextDoubleFromTo(0, cytoplasmDiameter);
		} while (
				((location[0] < nucleusEdgeLocationMax) && (location[0] > nucleusEdgeLocationMin))
				&&
				((location[1] < nucleusEdgeLocationMax) && (location[1] > nucleusEdgeLocationMin)));
	}
	
	/**
	 * Finds the location for placing an object in 2D space within the nucleus.
	 * 
	 * @param location list that will hold [x, y] coordinates for the new object 
	 * @param nucleusEdgeLocationMin coordinate [x] or [y] that marks the lowest
	 * value of the edge of the nucleus 
	 * @param nucleusEdgeLocationMax coordinate [x] or [y] that marks the highest
	 * value of the edge of the nucleus
	 */
	private void findLocationInNucleus(double[] location,
			double nucleusEdgeLocationMin, double nucleusEdgeLocationMax) {
		
		// ensures molecules only added to the nucleus
		location[0] = RandomHelper.nextDoubleFromTo(nucleusEdgeLocationMin, nucleusEdgeLocationMax);
		location[1] = RandomHelper.nextDoubleFromTo(nucleusEdgeLocationMin, nucleusEdgeLocationMax);
	}
}