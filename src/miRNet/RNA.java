/**
 * 
 */
package miRNet;

import repast.simphony.random.RandomHelper;
import repast.simphony.space.continuous.NdPoint;

/**
 * @author gl524
 *
 * Generic functions performed by RNA molecules.
 */
public abstract class RNA extends Component {
	
	/**
	 * Moves a miRNA/mRNA agent randomly in space.
	 * 
	 * @param movementRate the rate at which to move the RNA molecule
	 */
	public void moveRandomly(double movementRate) {
		NdPoint currentLocation = space.getLocation(this);
		double[] currentPosition = new double[] {currentLocation.getX(), currentLocation.getY()};
		double[] newPosition = new double[2];
		double movementVector;
		
		if (inCytoplasm) {
			do {
				// pick a direction to move
				movementVector = Math.toRadians(RandomHelper.nextDoubleFromTo(0.0, 360.0));
				// check that the new location is within allowed movement space
				newPosition[0] = currentPosition[0] + movementRate * Math.cos(movementVector);
				newPosition[1] = currentPosition[1] + movementRate * Math.sin(movementVector);
			} while (locatedInNucleus(newPosition[0], newPosition[1]));
		} else {
			// pick a direction to move
			movementVector = Math.toRadians(RandomHelper.nextDoubleFromTo(0.0, 360.0));
			// check that the new location is within allowed movement space
			newPosition[0] = currentPosition[0] + movementRate * Math.cos(movementVector);
			newPosition[1] = currentPosition[1] + movementRate * Math.sin(movementVector);
			
			if (!locatedInNucleus(newPosition[0], newPosition[1])) {
				inCytoplasm = true;
			}
		}
		space.moveByVector(this, movementRate, movementVector, 0);
	}
}
