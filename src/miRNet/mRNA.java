/**
 * 
 */
package miRNet;

import static java.awt.geom.Point2D.distance;
import miRNet.agents.Ribosome;
import repast.simphony.context.Context;
import repast.simphony.engine.watcher.Watch;
import repast.simphony.engine.watcher.WatcherTriggerSchedule;
import repast.simphony.query.InstanceOfQuery;
import repast.simphony.random.RandomHelper;
import repast.simphony.space.continuous.ContinuousSpace;
import repast.simphony.space.continuous.NdPoint;
import repast.simphony.util.ContextUtils;

/**
 * @author gl524
 *
 * mRNAs look for a foci in the cell where they can localise. At the foci
 * mRNAs can be targeted by the RISC complex within a certain period of time.
 * If the mRNA is not found by RISC, some of it will continue to translation
 * by associating with the nearest possible ribosome; some of the mRNA will
 * remain at the foci and become available for RISC binding.
 */
public abstract class mRNA extends RNA {
	
	// mRNA specific parameters accessible to watchers
	public boolean boundByRibosome;
	
	// mRNA specific parameters
	public abstract int getProbabilityToBeSilenced();
	protected boolean enteredTranslation;
	protected Foci targetFoci;
	protected boolean atFociLocation;
	protected boolean boundByRISC;
	protected Ribosome targetRibosome;
	protected Ribosome previousRibosome;
	
	// mRNA specific values
	protected final int movementRate = (Integer)parameters.getValue("mRNAMovementRate");
	protected int currentTimeAtFoci;
	protected final int waitingTimeAtFoci = (Integer)parameters.getValue("waitingTimeAtFoci");
	protected final int probabilityToEnterTranslation = (Integer)parameters.getValue("probabilityToEnterTranslation"); // x in 100 chance for the mRNA at foci to enter translation at the end of the waitingTimeAtFoci
	protected double lifeSpan;
	protected int meanLifeSpan;
	protected final double deviationOfLifeSpan = 0.1 * meanLifeSpan;
	
	/**
	 * Describes the generic activity of miRNAs.
	 */
	protected void mRNAActivity() {
		// move if not bound by RISC
		if (!boundByRISC) {
			// stay at the foci if not entered translation, otherwise move towards a ribosome
			if (!enteredTranslation) {
				// first locate a foci to move towards if not done so yet
				if (targetFoci == null) {
					targetFoci = locateFoci(space);
				}
				
				moveTowardsFoci();
				
				// if at the foci, increment the time step being there
				if (atFociLocation) {
					currentTimeAtFoci ++;
					
					// if the time at foci runs out, make a decision to enter translation or remain at the foci
					if (currentTimeAtFoci >= waitingTimeAtFoci) {
						tryToEnterTranslation();
					}
				}
			} else {
				// only move towards a ribosome if not yet bound to it
				if (!boundByRibosome) {
					moveTowardsRibosome();
				}
			}
		}
		
		if (!boundByRISC) {
			checkLifeSpan(lifeSpan, meanLifeSpan, deviationOfLifeSpan);
		}
		lifeSpan ++;
	}
	
	/**
	 * Perform movement towards a foci.
	 */
	protected void moveTowardsFoci() {
		NdPoint currentLocation = space.getLocation(this);
		NdPoint fociLocation = space.getLocation(targetFoci);
		
		// calculate allowed movement towards the foci
		double[] movementResult = findNewLocationByDirectedMovement(space, currentLocation, movementRate, fociLocation);
		
		// assume foci location and allow RISC association
		if (movementRate >= movementResult[3]) {
			if (!atFociLocation) {
				atFociLocation = true;
			}
		}
		
		// establish where to move depending on location restrictions
		if (inCytoplasm) {
			while (locatedInNucleus(movementResult[0], movementResult[1])) {
				double[] newMovementResult = findNewLocationByDirectedMovement(space, currentLocation, movementRate, fociLocation);
				movementResult = newMovementResult;
			}
		} else {
			if (!locatedInNucleus(movementResult[0], movementResult[1])) {
				inCytoplasm = true;
			}
	    }
		
		// make the required move towards the foci
		space.moveByVector(this, movementRate, movementResult[2], 0);
	}
	
	/**
	 * Perform the movement towards the nearest ribosome if possible, otherwise move randomly.
	 */
	protected void moveTowardsRibosome() {
		NdPoint currentLocation = space.getLocation(this);
		
		// find nearest unoccupied ribosome, only do this once
		targetRibosome = findRibosome(space, currentLocation);
		
		if (targetRibosome != null) {
			NdPoint ribosomeLocation = space.getLocation(targetRibosome);
			
			// calculate allowed movement towards the nearest Ribosome depending on state
			double[] movementResult = findNewLocationByDirectedMovement(space, currentLocation, movementRate, ribosomeLocation);
			
			// bind ribosome if close enough, otherwise move towards nearest one
			if (movementRate >= movementResult[3]) {
				space.moveTo(this, ribosomeLocation.getX(), ribosomeLocation.getY());
				boundByRibosome = true;
				previousRibosome = targetRibosome;
			} else {
				// make sure the movement is permitted, calculate a new one otherwise; set appropriate flags
				if (inCytoplasm) {
					while (locatedInNucleus(movementResult[0], movementResult[1])) {
						double[] newMovementResult = findNewLocationByDirectedMovement(space, currentLocation, movementRate, ribosomeLocation);
						movementResult = newMovementResult;
					}
				} else {
					if (!locatedInNucleus(movementResult[0], movementResult[1])) {
						inCytoplasm = true;
					}
				}
			// make the required move towards the ribosome
			space.moveByVector(this, movementRate, movementResult[2], 0);
			}
		} else {
			moveRandomly(movementRate);
		}
	}
	
	/**
	 * Attempts to change mRNA state to allow for it to be translated instead of being stored. 
	 */
	protected void tryToEnterTranslation() {
		int result = RandomHelper.nextIntFromTo(0, 100);
		if (result < probabilityToEnterTranslation) {
			enteredTranslation = true;
		} else {
			// reset at the foci timer
			currentTimeAtFoci = 0;
		}
	}
	
	/**
	 * Finds the nearest unbound mRNA and return its object. If no
	 * mRNA are available, returns null. 
	 * 
	 * @param space continuous space object
	 * @param location the current location of thisAgent
	 * @return nearest unbound ribosome, if possible
	 */
	protected Ribosome findRibosome(ContinuousSpace space, NdPoint location) {
		
		double nearestDistance = cytoplasmDiameter;
		Ribosome nearestTarget = null;
		
		// query the space around the agent
		Context<Object> context = ContextUtils.getContext(this);
		InstanceOfQuery result = new InstanceOfQuery(context, Ribosome.class);
		Iterable targets = result.query();
		
		// find all the unbound ribosomes of interest, store the nearest to the agent
		for (Object target : targets) {
			if (target instanceof Ribosome && !((Ribosome) target).getIsBound()) {
				NdPoint nearestLocation = space.getLocation(target);
				double newDistance = distance(location.getX(), location.getY(), 
						nearestLocation.getX(), nearestLocation.getY());
				
				if (newDistance < nearestDistance &&
						(Ribosome) target != previousRibosome) {
					nearestDistance = newDistance;
					nearestTarget = (Ribosome) target;
				}
			}
		}
		return nearestTarget;
	}
	
	/**
	 * Finds a foci within the cell towards which the mRNA needs to move.
	 * 
	 * @param space continuous space object where the molecule 
	 * needs to be stored
	 * @return one random foci
	 */
	protected Foci locateFoci(ContinuousSpace space) {
		Foci fociTarget = null;
		
		// find the foci in the simulation
		Context<Object> context = ContextUtils.getContext(this);
		Iterable<Object> target = context.getRandomObjects(Foci.class, 1);
		for (Object theOne : target) {
			fociTarget = (Foci) theOne;
		}
		
		return fociTarget;
	} 
	
	/**
	 * Watches the completion of translation and in response changes the state
	 * of the mRNA agent to unbound.
	 */
	@Watch(watcheeClassName = "miRNet.agents.Ribosome",
			watcheeFieldNames = "finished", 
			triggerCondition = "$watchee.finished == true",
			query = "within 0.0001", 
			whenToTrigger = WatcherTriggerSchedule.IMMEDIATE)
	public void releasedFromRibosome() {
		boundByRibosome = false;
	}
	
	/**
	 * Triggered by RISC binding. Starts RISC-mediated silencing.
	 */
	@Watch(watcheeClassName = "miRNet.agents.Protein_AGO2",
			watcheeFieldNames = "boundMRNA",
			triggerCondition = "$watchee.boundMRNA == true",
			query = "within 0.0001",
			whenToTrigger = WatcherTriggerSchedule.IMMEDIATE)
	public void RISCBinding() {
		boundByRISC = true;
	}
	
	/**
	 * Triggered by RISC dissociation. Releases the mRNA.
	 */
	@Watch(watcheeClassName = "miRNet.agents.Protein_AGO2",
			watcheeFieldNames = "boundMRNA",
			triggerCondition = "$watchee.boundMRNA == false",
			query = "within 0.0001",
			whenToTrigger = WatcherTriggerSchedule.IMMEDIATE)
	public void RISCDissociation() {
		boundByRISC = false;
	}
	
	/**
	 * Getter method for inCytoplasm.
	 * 
	 * @return true if the mRNA is located in the cytoplasm
	 */
	public boolean getInCytoplasm() {
		return inCytoplasm;
	}
	
	/**
	 * Getter method for enteredTranslation.
	 * 
	 * @return true if the mRNA has entered translation 
	 */
	public boolean getEnteredTranslation() {
		return enteredTranslation;
	}
	
	/**
	 * Getter method for boundByRISC.
	 * 
	 * @return true if the mRNA already bound by RISC 
	 */
	public boolean getBoundByRISC() {
		return boundByRISC;
	}
	
}
