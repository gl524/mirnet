/**
 * MiReN v1.1 (dev-name: miRNet)
 * 
 * Agent-based model of a miRNA regulatory network. Simulates the silencing
 * events of 2 mRNAs under the regulation of a single miRNA (miR-132) and a
 * non-targeting miRNA (miR-221). EP300 has been previously confirmed as a
 * target of miR-132 (Lagos, 2010) and AGO2 has been a recently established
 * target in lymphatic endothelial cells.
 * 
 * Date: 16-09-2014
 * Contact: german.leonov@york.ac.uk
 */
package miRNet;

import miRNet.agents.*;
import miRNet.base.DataCrawler;
import repast.simphony.context.Context;
import repast.simphony.context.space.continuous.ContinuousSpaceFactory;
import repast.simphony.context.space.continuous.ContinuousSpaceFactoryFinder;
import repast.simphony.dataLoader.ContextBuilder;
import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.parameter.Parameters;
import repast.simphony.query.InstanceOfQuery;
import repast.simphony.random.RandomHelper;
import repast.simphony.space.continuous.ContinuousSpace;
/**
 * @author German Leonov
 * 
 * Constructor class for the simulation.
 */
public class miRNetBuilder implements ContextBuilder<Object> {
	
	public static int seed;
	
	/** 
	 * Builds the simulation 2D space and populates it with
	 * initial agents numbers. Returns the context of the
	 * simulation environment which contains all objects.
	 */
	@Override
	public Context build(Context<Object> context) {
		context.setId("miRNet");
		
		// load parameters and the custom data crawler
		Parameters parameters = RunEnvironment.getInstance().getParameters();
		context.add(new DataCrawler());
		
		// set the simulation seed explicitly to ensure reproducability
		seed = (Integer)parameters.getValue("randomSeed");
		RandomHelper.setSeed(seed);
		
		// set termination time
		double terminationTime = (Double)parameters.getValue("terminationTime");
		RunEnvironment.getInstance().endAt(terminationTime);
		
		// create continuous space compartments
		double cellSize = (Double)parameters.getValue("cellSize");
		ContinuousSpaceFactory spaceFactory = ContinuousSpaceFactoryFinder.createContinuousSpaceFactory(null);
		ContinuousSpace<Object> cell = spaceFactory.createContinuousSpace("cell", context,
				new miRNet.base.MoleculeAdder<Object>(),
				new repast.simphony.space.continuous.BouncyBorders(),
				cellSize, cellSize);
		
		// population parameters and instantiation
		int fociCount = RandomHelper.nextIntFromTo((Integer)parameters.getValue("minFociCount"), (Integer)parameters.getValue("maxFociCount"));
		int ribosomeCount = (Integer)parameters.getValue("ribosomeCount");
		int miR132Count = (Integer)parameters.getValue("miR132Count");
		int miR221Count = (Integer)parameters.getValue("miR221Count");
		int mRNAAGO2Count = (Integer)parameters.getValue("mRNAAGO2Count");
		int mRNAEP300Count = (Integer)parameters.getValue("mRNAEP300Count");
		int proteinAGO2Count = (Integer)parameters.getValue("proteinAGO2Count");
		int proteinEP300Count = (Integer)parameters.getValue("proteinEP300Count");
		int proteinCREBCount = (Integer)parameters.getValue("proteinCREBCount");
		
		context.add(new Promoter(cell));
		
		for (int i = 0; i < fociCount; i++) {
			context.add(new Foci(cell));
		}
		
		for (int i = 0; i < ribosomeCount; i++) {
			context.add(new Ribosome(cell));
		}
		
		for (int i = 0; i < miR132Count; i++) {
			context.add(new miR132(cell));
		}
		
		for (int i = 0; i < miR221Count; i++) {
			context.add(new miR221(cell));
		}
		
		for (int i = 0; i < mRNAAGO2Count; i++) {
			context.add(new mRNA_AGO2(cell));
		}
		
		for (int i = 0; i < mRNAEP300Count; i++) {
			context.add(new mRNA_EP300(cell));
		}
		
		for (int i = 0; i < proteinAGO2Count; i++) {
			context.add(new Protein_AGO2(cell));
		}
		
		for (int i = 0; i < proteinEP300Count; i++) {
			context.add(new Protein_EP300(cell));
		}
		
		for (int i = 0; i < proteinCREBCount; i++) {
			context.add(new Protein_CREB(cell));
		}
		
		return context;
	}

}
