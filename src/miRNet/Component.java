/**
 * 
 */
package miRNet;

import static java.awt.geom.Point2D.distance;

import java.util.ArrayList;

import miRNet.agents.Promoter;
import miRNet.agents.Protein_AGO2;
import miRNet.base.DataCrawler;
import cern.jet.random.Normal;
import repast.simphony.context.Context;
import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.engine.schedule.ISchedule;
import repast.simphony.engine.schedule.ScheduleParameters;
import repast.simphony.essentials.RepastEssentials;
import repast.simphony.parameter.Parameters;
import repast.simphony.random.RandomHelper;
import repast.simphony.space.SpatialMath;
import repast.simphony.space.continuous.ContinuousSpace;
import repast.simphony.space.continuous.NdPoint;
import repast.simphony.util.ContextUtils;

/**
 * @author German Leonov
 *
 * Generic functions performed on molecules.
 */
public abstract class Component {
	
	// parameters here made public are accessible to all instantiated agent classes
	public static Parameters parameters = RunEnvironment.getInstance().getParameters();
	public ContinuousSpace<Object> space;
	protected boolean inCytoplasm;
	protected double movementRate;
	public static double activatedByPMA = 0;
	public static double PMATreatmentStart = (Integer)parameters.getValue("PMATreatmentStart");
	public Promoter targetPromoter;
	public DataCrawler dataCrawler;
	public static ArrayList<Protein_AGO2> AGO2ProteinsAvailableToMiRNAs = new ArrayList<Protein_AGO2>();
	
	protected double cytoplasmDiameter = (Double)parameters.getValue("cellSize");
	private double nucleusToCytoplasmRatio = 4;
	private double nucleusRadius = (cytoplasmDiameter / nucleusToCytoplasmRatio) / 2;
	protected double nucleusCenter = cytoplasmDiameter / 2; // proteins want to know where is centre of the cell for moving towards the nucleus
	private double nucleusEdgeLocationMin = nucleusCenter - nucleusRadius;
	private double nucleusEdgeLocationMax = nucleusCenter + nucleusRadius;
	
	
	/**
	 * Checks if the location(x, y) is within the nucleus
	 * 
	 * @param x coordinate [x] in 2D continuous space
	 * @param y coordinate [y] in 2D continuous space
	 * @return true if the location is in the nucleus
	 */
	public boolean locatedInNucleus(double x, double y) {
		return ((x < nucleusEdgeLocationMax) && (x > nucleusEdgeLocationMin))
				&&
				((y < nucleusEdgeLocationMax) && (y > nucleusEdgeLocationMin));
	}
	
	/**
	 * Evaluates the molecule's chance of remaining in the simulation and
	 * removes it if necessary.
	 * 
	 * @param lifeSpan the current lifespan
	 * @param meanLifeSpan the mean lifespan
	 * @param standardDeviation the deviation from the mean lifespan
	 */
	public void checkLifeSpan(double lifeSpan, double meanLifeSpan, double standardDeviation) {
		Context<Object> context = ContextUtils.getContext(this);
		
		// probe mean LifeSpan from a normal distirbution
		Normal gaussian = RandomHelper.createNormal(meanLifeSpan, standardDeviation);
		double suggestedLifeSpan = gaussian.nextDouble();
		
		// remove based on the probability 
		if (suggestedLifeSpan - lifeSpan < 0) {
			context.remove(this);
			
			// make sure to remove AGO2 (not bound by miR but lifeSpan expired) from available list
			if (this instanceof Protein_AGO2) {
				AGO2ProteinsAvailableToMiRNAs.remove(this);
			}
		}
	}
	
	/**
	 * Calculates where the molecule would end up in space if it tried to move. 
	 * 
	 * @param space continuous space object
	 * @param currentLocation 2D position of the molecule
	 * @param movementRate the increment by which the molecule can move per step
	 * @param targetLocation the molecule location towards which the current molecule
	 * needs to be moved towards
	 * @return movement result as a list of [new x, new y, vector, distance to target]
	 */
	public double[] findNewLocationByDirectedMovement(ContinuousSpace space, NdPoint currentLocation, double movementRate, NdPoint targetLocation) {
		
		double deviation = Math.toRadians(90.);
		
		double movementVector;
		double[] movementResult = new double[4];
		
		double deviationAngle = RandomHelper.nextDoubleFromTo(deviation * -1, deviation);
		movementVector = SpatialMath.calcAngleFor2DMovement(space, currentLocation, 
				targetLocation) + deviationAngle;
		double distanceToTarget = distance(currentLocation.getX(), currentLocation.getY(), 
				targetLocation.getX(), targetLocation.getY());
		
		movementResult[0] = currentLocation.getX() + movementRate * Math.cos(movementVector);
		movementResult[1] = currentLocation.getY() + movementRate * Math.sin(movementVector);
		movementResult[2] = movementVector;
		movementResult[3] = distanceToTarget;
		
		return movementResult;
	}
	
	/**
	 * Schedules an action for a specified time in the future, given a rate.
	 * 
	 * @param action name of the method to schedule, must be public
	 * @param rate the duration the scheduler will wait before carrying out the action
	 */
	public void performOverTimeAction(String action, double rate) {
		ISchedule schedule = RunEnvironment.getInstance().getCurrentSchedule();
        ScheduleParameters params = ScheduleParameters.createOneTime(RepastEssentials.GetTickCount() + rate,
        		ScheduleParameters.RANDOM_PRIORITY, ScheduleParameters.NO_DURATION);
        schedule.schedule(params, this, action);
	}
	
	/**
	 * Finds the promoter agent.
	 * 
	 * @return Promoter agent
	 */
	public Promoter getPromoter() {
		Context<Object> context = ContextUtils.getContext(this);
		Iterable<Object> targets = context.getObjects(Promoter.class);
		
		for (Object target : targets) {
			targetPromoter = (Promoter) target;
		}
		
		return targetPromoter;
	}
	
	/**
	 * Finds the DataCrawler agent.
	 * 
	 * @return DataCrawler agent
	 */
	public DataCrawler getDataCrawler() {
		Context<Object> context = ContextUtils.getContext(this);
		Iterable<Object> targets = context.getObjects(DataCrawler.class);
		
		for (Object target : targets) {
			dataCrawler = (DataCrawler) target;
		}
		
		return dataCrawler;
	}

}
